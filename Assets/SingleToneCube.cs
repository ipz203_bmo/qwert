using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleToneCube : MonoBehaviour
{

    /// <summary>
    /// ������������ ��������� ������� "������"
    /// �� ��������� ����-����������, �������� ��� �� ������ �������, ��� �������� ������ ����� � ������ ����.
    /// � ������� Start �� �������� ���� ��������� �� �������� ����.
    /// </summary>
 public static SingleToneCube Instance { get; private set; }

    private MeshRenderer _renderer; // ��������� ����� ��� ��������� ����������, �� ������� �� ����������� ������.
    

     void Start()
     {
        _renderer = GetComponent<MeshRenderer>(); //�������� ��� ��������� ����� ����� �� �����;
        SetColorState(false); // ��������� ���������� ����
        Instance = this; //���������� � �������� �����, ��� ������ ������ �� ��������� ����������.
     }

    public void SetColorState(bool state)
    {
        _renderer.material.color = (state == true) ? Color.green : Color.red;
        //������� ���� � ��������� �� ����� � ������.
    }
}

